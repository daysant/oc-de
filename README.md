# oc-de
OpenComputers Desktop Environment

## Installation
To install oc-de, simply open up OpenOS and type:<br>
```https://gitlab.com/daysant/oc-de/-/raw/main/installer.lua```<br>
The installer should install oc-de automatically.<br>
Before installation, make sure your system meets the following system requirements:
|Component    |                       |
|-------------|-----------------------|
|CPU          |Tier 2                 |
|GPU          |Tier 2<sup>12</sup>    |
|RAM          |2x Tier 2.0<sup>2</sup>|
|HDD          |Tier 2<sup>2</sup>     |
|Internet card|Yes                    |

1 - Tier 3 required for split windows<br>
2 - Higher tier needed for more programs<br>

## Developing apps for oc-de
oc-de apps differ from regular OpenOS apps in that they are a directory rather than a single ```.lua``` file.<br>
Proper oc-de apps always come with at least four files in one directory, a main Lua file (typically `main.lua`), an icon file (`icon.img`), a help file (`programname.hlp`), and a pointer to help OpenOS find the files (`programname.pnt`)<br>
Additional files may be used for callbacks, resources, or threaded functions.<br>
Other than the pointer file needing to be in the root directory of the program, you have completely free will over how the files are organised.<br>
It is suggested to divide files up accordingly, however.<br>
A typical directory may look like:
```/program
  program.pnt
  main.lua
  side.lua
  program.hlp
  /resources
    icon.img
    img1.img
    img2.img
```
